---
layout: post
title: "최강야구 15회 리뷰 및 16회 예고"
toc: true
---


## 최강야구 15회 리뷰 및 16회 예고

 안녕하세요. 최강야구 리뷰하는 산서고장입니다.
 오늘도 더더군다나 최강야구 15회 리뷰 및 16회 예고 하도록 하겠습니다.
 오늘은 평소와 다르게 우리 최강야구 팀의 막내 윤준호 선수와 류현인 선수의 `23년 신인 드래프트 결과로 시작하게 되었는데요.

 신인 드래프트를 신청한 두 선수는 본인인 만치 콩팥이 떨어져 나갈만큼 긴장했겠지만, 최강야구 제작진들을 비롯한 스텝, 및 출연진들도 기수 일처럼 관심을 가지고 드래프트를 시청했습니다. 결과는 어떻게 되었을까요?

 드래프트 지명의 기쁜소식은 선차 동의대 포수 윤준호로부터 시작되었는데요.
 5라운드에 두산베어스로 지명되었습니다. 축하드립니다.
 실시간으로 시청중이었던 최강야구 사무실은 꼭꼭 냄새 삯 것 만판 기쁨의 함성으로 난리가 났네요.

 최강 몬스터즈를 대표해서 캡틴 박용택 선수가 드래프트 현장에서 축하해주기도 했습니다.
 그리고... 프로야구 선수가 되기위해 애썼던 그간의 시간들이 떠올랐는지 우리의 윤준호 선수는 눈물을 주체하지 못합니다.

 다음음 7라운드 KT위즈에 의해 류현인 선수가 지명되었는데요. 7라운드 막바지까지 지명되지 않아 본인은 말할것도 없고 실시간으로 응원하는 최강야구 관계자들, 심지어 시청자까지 마음을 졸였습니다.

 종당 7라운드에서 지명되었고, 때마침 나타난 엄마를 보는 순간! 그사이 참았던 눈물이 왈칵 쏟아진 류현인 선수..
 그동안 고생 많았습니다. 시방 시작이기 그렇게 프로에서도 시시로 안면 보여주면서 신문 전해주시기 바랍니다.
 그러면서.. 최강야구 선수들의 드래프트시절을 회상하듯 보여줍니다.

 그렇습니다. 그들도 위의 두 선수와 같이 지명에 마음속 졸이던 아마추어였던 시절이 있었던 것이겠죠..

 이렇게 한편의 감동드라마가 지나가고
 종당 최강몬스터즈 VS 경남고의 2차전이 시작되었습니다.
 경기장은 참으로 라팍의 느낌 이승엽 감독의 고향인 대구 라이온즈파크인데요.

 시작 전 라인업의 특이사항으로는 4번타자에 정의윤 선수가 복귀한 것과 죽 3번타자로 박용택 선수가 기용되고 있다는 점인데요. 이러한 라인업을 통해 믿음의 혜택을 매우 본 믿음의 주관 이승엽 감독의 스타일이 느껴집니다.

 1회는 두 팀다 안정적인 투수력을 바탕으로 무실점으로 마쳤습니다. 각별히 투수도 투수이지만, 외야수들의 멋진 수비가 공격 팀들의 회심의 기회를 날리게 만들었네요.
 종단 선취점은 참말 최강 몬스터즈에서 가져가게 되었습니다.

 2회 선발타자로 나운 정의윤 선수의 행운의 내야안타를 시작으로

 추후 타자로 나온 이택근 선수. 지난 경남고 1차전으로 부절 안타는 마감했지만, 2차전에는 첫번째 타석에서 제대로 1-2루간을 가르는 안타를 처냅니다.

 게다가 천재 타자 정성훈이 상대자 투수의 직구를 받아쳐 중견수 키를 넘기는 2루타를 맞춰냅니다.

 이로서 출루해 있던 정의윤 선수와 이택근 선수가 싹쓸이로 홈인하게 되어 최강몬스터즈 2 : 경남고 0의 점수차가 만들어 집니다.
 게다가 경남고의 위기는 여기서 끝나지 않습니다.

 2점 선취점을 빼앗긴 익금 후 이홍구, 김문호 등이 볼넷으로 출루하자 경남고의 에이스 신영우선수를 투입하게 됩니다.
 주자 1, 2루 상황에서 마운드에 오른 신영우 투수는 몸이 아직껏 덜 풀렸는지 정근우와의 풀카운트 승부에서 볼넷으로 출루를 시키게 됩니다.

 금방 맞게 되는 만루 상황에서 여전한 제구 난조의 문제로 최수현 선수를 사구로 내보내며, 1점을 실점한 경남고,, 여기까지 점수차 최강몬스터즈3 : 경남고0 점입니다.
 이렇게 분위기가 최강몬스터즈에게 넘어간 기간 경남고의 반격이 시작됩니다.

 비가 와서 그런지.. 송승준 선수의 컨디션이 금방 좋지 못합니다. 그래서 빠른공 승부보다는 맞춰잡는 승부를 선택하게 되는데요.

 이러한 전략에 뒷받힘해야 할 내야수 최수현 선수와 정근우 선수가 2번에 거쳐 실책을 하고 맙니다.
 승리하고 있던 분위기는 오간대 가난히 사라지고, 결과는 알수 없는 미궁에 빠지게 되는데요..

 미리감치 내야수들의 실책으로 무사 주자 1, 3루 상황에서 경남고 선수들이 경기의 흐름을 잡고 득점찬스를 놓치지 않는데요. 7번타자 장수원 선수의 내야 안타로 3루주자가 홈인하게 되어 3:1의 점수차를 만들어 냅니다.

 아울러 8번타자 배정훈 선수의 내야를 뚫는 안타로 2점을 한결 추가하는 경남고. 이렇게 되어 점수차는 3:3 동점이 됩니다.

 이러한 내야수의 부진을 스트라이크를 통해 해결하는 송승준 선수의 클라스를 볼 운명 있었습니다.

 4회에 들어 이홍구 선수가 게다가 안타로 출루하게 되었습니다. 이홍구 선수가 현대 포수의 포지션을 받고 출전했는데, 만손 아웃은 못시켰지만, 견제를 위해 2루로 과감하게 투구하는 모습을 보며, 입스를 어느정도 극복해 가고 있다는 것을 느낄 요체 있었습니다.

 

 익금 뒤끝 김문호 선수의 볼넷 출루와 정근우 선수의 천운의 내야 안타를 통해 이홍구 선수가 홈까지 무리하지 않고 들어오게 됩니다. 이에 점수차는 4:3으로 최강몬스터즈가 또다시 앞서가기 시작합니다.

 뿐만 아니라 5회 말까지 송승준 선수가 본인의 클라스를 [야구입스](https://scarfdraconian.com/sports/post-00028.html) 보이며, 온전하지 않는 컨디션을 극복하며 퀄리티 게임으로 선발승 요건을 갖추며 이닝을 마무리 하게 됩니다.

 곧바로 두번째 투수로 등판한 오주원 선수, 위력적인 체인지업의 무브먼트가 인상적이라 할 핵심 있었는데요.

 그다지 빠르지 않은 공으로도, 칼같은 제구와 볼배압, 더욱이 결정구 체인지업을 바탕으로 8회까지 무실점하게 됩니다.

 

 게다가 9회, 지난번 경기에 어서 이승엽 감독이 다시한번 등판하게 됩니다.

 지난번과 같은 사시나무 투수와 승부하게 되었는데요. 산재까치 사시나무 투수는 무슨 가문의 영광인지 모르겠네요.
 아무튼, 지난번 내야수에게 아웃당했던 것과 달리 이번에는 볼넷으로 출루할 핵심 있었습니다.
 물론 득점으로 이어지지는 못했지만요..

 9회 마운드에 오른 이대은 투수, 마무리투수로서의 임무를 부여받았는데요. 믿어주는 만큼의 결과를 보이기에 이릅니다.

 본인의 현역 시절로 돌아온 듯한 산업 스피드 149km!!!!
 마침내 최강몬스터즈에도 파이어볼러가 탄생하게 되었습니다. 거연히 이러다 거듭 현역으로 점방 되는게 아닌가 하는 생각이 들어가게 되었습니다.

 최강야구라는 프로그램을 보면서 장부 보람되는 장면이 이런 장면인 것 같습니다.
 즉각 노력을 통해 본인의 어려움과 한계를 극복하는 모습이죠!!
 경기를 드라마틱하게 이기는것도 프로그램상 필요한 요소라 볼 행우 있겠지만.. 이대은 선수의 이번 투구처럼 자신의 역경을 이겨내는 스토리도 최강야구 경륜 취지와 잘 어울리고, 의도했던 만큼 시청자로 하여금 감동을 받게 만듭니다.

 종내 경남고와의 2차전은 최강몬스터즈의 승리로 맺음말 하게 되었습니다.
 오늘의 MVP는 팔에 통증이 있음에도 불구하고 5회까지 역투한 끝에 선발승을 따낸 송승준 선수에게 돌아가게 되었습니다.
 이로서 최강몬스터즈는 12승 10승 2패로 승률 8할 3푼 3리에 이릅니다.

 원래는 U-18국가대표와 겨룸 전에 독립야구단과의 경기가 있었고 언론 편성도 되어 있었는데요. 많은 사람들이 U-18국가대표와의 경기를 즉각 방영해달라고 요청했나 봅니다.
 때문에 다음 16회는 최강 몬스터즈 VS U-18 국가대표의 경기가 새롭게 편성되었습니다.
 벌써부터 기대가 되네요.. 당연히 다녀오신 분들은 결과를 알고 계시겠지만요..ㅜㅜ

 지금부터는 16회차의 운동경기 예고입니다. 나레이션에 나와있 듯 다시는 많은 관중 앞에서 게임을 브뤼케 못할 줄 알았던 선수들이 되처 많은 관객들 앞에서 게임을 하게 되자 감격받아하는것 같았는데요..
 소득 시대 표를 발매하자 마자 대다수 팔렸던게 생각나네요.. ㅜㅜ

 강팀을 만나게 되어 마음의 각오를 새롭게 했을까요? 겨룸 초에는 최강몬스터즈가 경기분위기를 이끌어가는 내용으로 보여집니다.

 반면에 이대로 당하고만 있지 않는 U-18대표팀!
 드래프트 1번 김서현 투수를 비롯해서, 최강몬스터즈를 꽁꽁 묶에 고전케 만들었던 신영우, 윤영철 선수까지 등판하게 됩니다.

 게다 벌써 다리파 팀별로 드래프트 우선순위로 뽑힌 타자들의 매서운 방망이 질이 시작된 듯 보입니다.

 이러한 가운데, 류현인 선수가 3루 도루에 성공하면서, 최강몬스터즈가 되처 흐름을 가져오려는 모습을 보이며 예고편이 마무리되게 되는데요.
 이후 등판한 윤영철 선수를 최강몬스터즈 선수들이 극복해 낼 복운 있었을까요.. 궁금해집니다.
 

 최강야구 15회 리뷰 및 16회 예고는 여기까지 였습니다. 최강몬스터즈의 승리가 나의 승리인 것 마냥 대용 감격 하며 테두리 미팅 경계 미팅 보게 되는 것 같습니다.

 더군다나 선수들 개개인이 어려움을 극복해 내는 과정들이 나도 할 생령 있다는 용기와 감동을 주기도 합니다.
 버금 주 강팀을 난점 결과를 시청자들이 보게 될텐데.. 딴은 어떻게 될지 궁금하며,, 이왕이면 최강몬스터즈가 이겼으면 좋겠다고 마음속에 응원합니다.
 

 끝막음 산서고장이었습니다.
